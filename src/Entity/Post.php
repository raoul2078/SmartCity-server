<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"group"})
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Groups({"group"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"group"})
     */
    private $content;

    /**
     * @var \App\Entity\User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @Groups({"group"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Groups({"group"})
     */
    private $createdAt;

    /**
     * @var \App\Entity\SocialGroup
     * @ORM\ManyToOne(targetEntity="SocialGroup", inversedBy="posts")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $group;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="post", cascade={"persist"})
     * @Groups({"group"})
     */
    private $comments;

    public function __construct() {
        $this->comments = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return \App\Entity\SocialGroup
     */
    public function getGroup() {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group): void {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void {
        $this->content = $content;
    }

    /**
     * @return \App\Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void {
        $this->createdAt = $createdAt;
    }



    public function addComment(Comment $comment) {
        $this->comments[] = $comment;
    }

    public function removeComment(Comment $comment) {
        $this->comments->removeElement($comment);
    }

    /**
     * @return mixed
     */
    public function getComments() {
        return $this->comments;
    }


}
