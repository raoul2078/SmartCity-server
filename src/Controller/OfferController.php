<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;

class OfferController extends Controller {
    /**
     * Retourne la liste des offres commerciales
     * @Rest\View(serializerGroups={"offer"})
     * @Rest\Get("/api/offers")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Offer[]|\App\Entity\Trading[]|array
     */
    public function getOffersAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);

        /** @var \App\Entity\User $user */
        $user = $token->getUser();
        $interests = [];
        $tradings = [];
        if ($user->getLocation() == null) {
            return [];
        }
        $location = $user->getLocation()->getId();
        foreach ($user->getInterests() as $interest) {
            $interests[] = $interest->getId();
        }
        foreach ($user->getTradings() as $trading) {
            $tradings[] = $trading->getId();
        }
        $offers = $em->getRepository('App:Offer')
            ->findOffersByUserInterests($interests, $location, $tradings);
        return $offers;
    }

    /**
     * Retourne la liste des offres commerciales
     * @Rest\View(serializerGroups={"offer"})
     * @Rest\Get("/api/offers/{name}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\Offer[]|\App\Entity\Trading[]|array
     */
    public function getOffersByNameAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $token = $em->getRepository('App:AuthToken')
            ->findOneBy([
                'value' => $request->headers->get('X-Auth-Token')
            ]);

        /** @var \App\Entity\User $user */
        $user = $token->getUser();
        $location = $user->getLocation() == null ? null : $user->getLocation()->getId();
        $offers = $em->getRepository('App:Offer')
            ->findOffersByName($request->get('name'), $location);
        return $offers;
    }
}
