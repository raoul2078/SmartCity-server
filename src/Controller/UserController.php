<?php

namespace App\Controller;

use App\Entity\AuthToken;
use App\Entity\Location;
use App\Entity\User;
use App\Form\UserType;
use App\Utils\FormErrors;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller {

    use FormErrors;

    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Get("/api/users")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return array
     */
    public function getUsersAction(Request $request) {
        $raoul = new User();
        $raoul->setId(1);
        $raoul->setFirstname("Raoul");
        $raoul->setLastname("Adjakly");
        $raoul->setDateOfBirth(new \DateTime('1998-01-20'));
        $raoul->setEmail("uwiadjakly@gmail.com");
        $raoul->setPassword("123456");

        $steph = new User();
        $steph->setId(2);
        $steph->setFirstname("Steph");
        $steph->setLastname("Curry");
        $steph->setDateOfBirth(new \DateTime('1988-03-14'));
        $steph->setEmail("steph@gmail.com");
        $steph->setPassword("123456");
        $users = [$raoul, $steph];
        return $users;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED, serializerGroups={"auth-token"})
     * @Rest\Post("/api/users")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\AuthToken|\FOS\RestBundle\View\View
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function postUsersAction(Request $request) {
        $user = new User();
        $form = $this->createForm(UserType::class, $user, [
            'validation_groups' => [
                'Default',
                'New'
            ]
        ]);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($encoded);
            $user->setRoles(['ROLE_USER']);
            $authToken = new AuthToken();
            $authToken->setValue(base64_encode(random_bytes(50)));
            $authToken->setCreatedAt(new \DateTime('now'));
            $authToken->setUser($user);

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->persist($authToken);
            $em->flush();
            return $authToken;
        }
        else {
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Put("/api/users/{id}")
     */
    public function updateUserAction(Request $request) {
        return $this->updateUser($request, true);
    }

    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Patch("/api/users/{id}")
     */
    public function patchUserAction(Request $request) {
        return $this->updateUser($request, false);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param $clearMissing
     * @return \App\Entity\User|null|object|\Symfony\Component\Form\FormInterface|static
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function updateUser(Request $request, $clearMissing) {
        $em = $this->get('doctrine.orm.entity_manager');

        $user = $em
            ->getRepository('App:User')
            ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $user User */

        if (empty($user)) {
            return $this->userNotFound();
        }

        if ($clearMissing) { // Si une mise à jour complète, le mot de passe doit être validé
            $options = ['validation_groups' => ['Default', 'FullUpdate']];
        }
        else {
            $options = []; // Le groupe de validation par défaut de Symfony est Default
        }

        $form = $this->createForm(UserType::class, $user, $options);

        $form->submit($request->request->all(), $clearMissing);

        if ($form->isValid()) {
            // Si l'utilisateur veut changer son mot de passe
            if (!empty($user->getPlainPassword())) {
                $encoder = $this->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPlainPassword());
                $user->setPassword($encoded);
            }

            //Mise à jour de la ville de l'utilisateur

            if (!empty($request->get('location'))) {
                $city = ($request->get('location'))['city'];
                $country = ($request->get('location'))['country'];
                $location = $em->getRepository('App:Location')->findOneBy([
                    'city' => $city,
                    'country' => $country
                ]);
                if ($location) {
                    $user->setLocation($location);
                }
                else {
                    $newLocation = new Location();
                    $newLocation->setCity($city);
                    $newLocation->setCountry($country);

                    $user->setLocation($newLocation);
                }
            }
            $em->flush();
            return $user;
        }
        else {
            //return $form;
            return View::create($this->getErrors($form), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Patch("/api/users/{id}/interests")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateUserInterestsAction(Request $request) {
        $user = $this->get('doctrine.orm.entity_manager')
            ->getRepository('App:User')
            ->find($request->get('id'));
        $interests = $request->get('interests');
        $sports = in_array('football', $interests)
            || in_array('nba', $interests)
            || in_array('tennis', $interests)
            || in_array('natation', $interests);
        if ($sports) {
            $interests[] = 'sport';
        }

        if ($user) {
            $user->setInterests(new ArrayCollection());
            $em = $this->get('doctrine.orm.entity_manager');
            $interestList = $em->getRepository('App:Interest')
                ->getByCategories($interests);
            foreach ($interestList as $item) {
                $user->addInterest($item);
            }
            $em->merge($user);
            $em->flush();
            return $user;
        }
        return $this->userNotFound();
    }

    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Post("/api/users/{id}/tradings")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return static
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addUserTradingAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em
            ->getRepository('App:User')
            ->find($request->get('id'));
        $tradings = $request->get('tradings');
        if (empty($user)) {
            return $this->userNotFound();
        }
        $tradingList = $em->getRepository('App:Trading')->getByIds($tradings);
        foreach ($tradingList as $item) {
            if (!$user->containsTrading($item->getId())) {
                $user->addTrading($item);
            }
        }
        $em->flush();
        return $user;
    }

    /**
     * @Rest\View(serializerGroups={"user"})
     * @Rest\Delete("/api/users/{id}/tradings")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\User|null|object|static
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeUserTradingAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em
            ->getRepository('App:User')
            ->find($request->get('id'));
        $tradings = $request->get('tradings');
        if (empty($user)) {
            return $this->userNotFound();
        }
        $tradingList = $em->getRepository('App:Trading')->getByIds($tradings);
        foreach ($tradingList as $item) {
            $user->removeTrading($item);
        }
        $em->flush();
        return $user;
    }

    /**
     * @Rest\View(serializerGroups={"trading"})
     * @Rest\Get("/api/users/{id}/tradings")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function getUserTradingsAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em
            ->getRepository('App:User')
            ->find($request->get('id'));
        if (empty($user)) {
            return $this->userNotFound();
        }
        return $user->getTradings();
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"user"})
     * @Rest\Post("/api/users/{id}/profile-picture")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \App\Entity\User|\FOS\RestBundle\View\View|null|object
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postUserProfileAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('App:User')
            ->find($request->get('id'));
        if ($user) {
            $oldPicture = basename($user->getPicture());
            $dir = $_SERVER['DOCUMENT_ROOT'] . '/SmartCity-server/public/img/' . $user->getId();
            $path = $dir . '/' . $oldPicture;

            if (!is_dir($dir)) {
                mkdir($dir);
            }
            if (file_exists($path)) {
                unlink($path);

            }
            $tmp = explode(".", $_FILES['upload']['name']);
            $extension = end($tmp);
            $filename = 'profile.' . $extension;

            $file_path = dirname(__FILE__, 3) . '/public/img/' . $user->getId() . '/' . $filename;
            $error = false;
            if (move_uploaded_file($_FILES['upload']['tmp_name'], $file_path)) {
                $user->setPicture($user->getId() . '/' . $filename);
                $em->flush();
            }
            else {
                $error = true;
            }
            if ($error) {
                return View::create(['message' => 'Unable to save file'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            return $user;
        }
        return $this->userNotFound();
    }


    /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/api/auth-tokens/{id}")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function removeAuthTokenAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $authToken = $em->getRepository('App:AuthToken')
            ->find($request->get('id'));
        /* @var $authToken AuthToken */

        $connectedUser = $this->get('security.token_storage')
            ->getToken()
            ->getUser();

        if ($authToken && $authToken->getUser()
                ->getId() === $connectedUser->getId()) {
            $em->remove($authToken);
            $em->flush();
        }
        else {
            throw new BadRequestHttpException();
        }
    }


    /**
     * @Rest\View(serializerGroups={"advert"})
     * @Rest\Get("/api/users/{id}/adverts")
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function getUserAdverts(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('App:User')
            ->find($request->get('id'));
        if (empty($user)) {
            return $this->userNotFound();
        }
        $interests = [];
        foreach ($user->getInterests() as $interest) {
            $interests[] = $interest->getId();
        }

        $adverts = $em->getRepository('App:Advert')
            ->findByCategories($interests);
        return $adverts;
    }

    private function userNotFound() {
        return View::create(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
    }

}
