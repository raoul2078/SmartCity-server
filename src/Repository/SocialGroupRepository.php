<?php

namespace App\Repository;

use App\Entity\SocialGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SocialGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method SocialGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method SocialGroup[]    findAll()
 * @method SocialGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocialGroupRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, SocialGroup::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('g')
            ->where('g.something = :value')->setParameter('value', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    public function findGroupsByName($name) {
        return $this->createQueryBuilder('g')
            ->where('g.name LIKE :name OR g.description LIKE :name')
            ->setParameter('name', '%'.$name.'%')
            ->orderBy('g.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
