<?php

namespace App\Repository;

use App\Entity\Advert;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * @method Advert|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advert|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advert[]    findAll()
 * @method Advert[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertRepository extends ServiceEntityRepository {
    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Advert::class);
    }

    public function findUserAdverts(int $userId, int $page = 1) {
        $query = $this->createQueryBuilder('a')
            ->where('a.user = :val')
            ->setParameter('val', $userId)
            ->getQuery();
        return $this->createPaginator($query, $page);
    }

    private function createPaginator($query, int $page): Pagerfanta {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($query));
        $paginator->setMaxPerPage(10);
        $paginator->setCurrentPage($page);
        return $paginator;
    }

    public function findByCategories($categories) {
        return $this->createQueryBuilder('a')
            ->where('a.category IN (:value)')->setParameter('value', $categories)
            ->getQuery()
            ->getResult()
            ;
    }
    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('a')
            ->where('a.something = :value')->setParameter('value', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
