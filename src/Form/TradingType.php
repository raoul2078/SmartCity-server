<?php

namespace App\Form;

use App\Entity\Location;
use App\Entity\Trading;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TradingType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('id', null, ['required' => false])
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control mb-2']])
            ->add('completeAddress', TextType::class, [
                'required' => true,
                'label' => 'Adresse'
            ])
            ->add('streetNumber', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['hidden' => true]
            ])
            ->add('route', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['hidden' => true]
            ])
            ->add('postalCode', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['hidden' => true]
            ])
            ->add('latitude', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['hidden' => true]
            ])
            ->add('longitude', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => ['hidden' => true]
            ])
            ->add('location', LocationType::class, [
                'label' => false,
                'attr' => ['id' => 'location', 'hidden' => true],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Trading::class,
            'csrf_protection' => false
        ]);
    }
}
