<?php

namespace App\Form;

use App\Entity\Offer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('id', null, ['required' => false])
            ->add('product', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Produit',
                'required' => false
            ])
            ->add('brand', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Marque',
                'required' => false
            ])
            ->add('price', TextType::class, [
                'attr' => ['class' => 'form-control'],
                'label' => 'Prix',
                'required' => false
            ])
            ->add('category', TextType::class, ['attr' => ['class' => 'form-control mt-3']])
            ->add('file', FileType::class, [
                'label' => 'Image',
                'required' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Offer::class,
            'csrf_protection' => false
        ]);
    }
}
