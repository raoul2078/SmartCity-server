<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('id', null, ['required' => false])
            ->add('firstname')
            ->add('lastname')
            ->add('plainPassword', null, ['required' => false])
            ->add('email', EmailType::class)
            ->add('dateOfBirth', DateType::class, [
                'required' => false,
                'format' => 'yyyy-MM-dd',
                'widget' => 'single_text'
            ])
            ->add('authWithGoogle', null, ['required' => false])
            ->add('picture', null, [
                'required' => false,
                'empty_data' => 'default-profile.png'
            ])
            ->add('location', LocationType::class, [
                'required' => false,
            ])
            ->add('roles', null, ['required' => false])
            ->add('fcmToken', null, ['required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => User::class,
            'csrf_protection' => false
        ]);
    }
}
